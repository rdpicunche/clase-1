package cl.duoc.claseuno;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSaludos;
    private EditText etSaludos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSaludos = (Button) findViewById(R.id.btnSaludos);
        etSaludos=(EditText) findViewById(R.id.etSaludos);
       /* btnSaludos.setOnClickListener(new View.OnClickListener() {

                                          @Override
                                          public void onClick(View v) {
                                              Toast.makeText(LoginActivity.this, "Hola mundo!!!", Toast.LENGTH_LONG).show();
                                          }
                                      }
        );*/

        btnSaludos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(LoginActivity.this, "Wenaaa  " + etSaludos.getText().toString() + "!!!", Toast.LENGTH_LONG).show();
    }
}
